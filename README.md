﻿# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="appearance.png" width="1024px" height="768px"></img>

## features
* 屬性樣板切換與舒適的設計界面
* 筆刷與像擦工具可以設定透明度或強度
* 文字可以先輸入變更樣式再按enter套用
* 文字可以更改斜體或粗體
* 支援數十種文字字型
* 上傳圖片後可以再更改寬高比例


## known bugs
* 部分自型無法用斜體
* 文字輸入時直接切換工具會留下框線
* 步驟回覆在新建又前進會直接回復到更之前的狀態
* 作畫時往canvas移出會無法記錄步驟
* 作畫時超出canvas會斷掉，必須再按一次
* 用半透明的筆刷/像擦倒退會出現奇怪半透明行為
* 圓圈只能上往下畫，否則位置不如預期
* 三角形只能往右畫，否則形狀不如預期

## functions description
1.init: 呼叫所有初始化函式
2.brush_hover、brush_unohver...: 控制工具列按鈕行為
3.switch_property、close_property: 隱藏與展開工具屬性面板
4.mouse_move、mouse_down、mouse_up: 處理canvas上的滑鼠事件
5.drawTextOnCanvas、changeTextboxFont...雜項，輔助上述函式

