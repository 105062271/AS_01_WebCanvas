﻿// Operational variables & onload functions
var buttonState = [false, false, false, false, false],
    mouseDown = false,
    cX = 0, cY = 0,     //current & previous coordinate
    pX = 0, pY = 0;
    entering = false;

function init() {
    init_buttons();
    init_tools();
    init_properties();
    push_step_stack()   //push empty canvas first
}

// Tools variables
var brushSize,
    brushOpacity,
    brushColor;

var eraserSize,
    eraserDepth;

var textFont,
    textSize,
    textColor,
    textIsBold,
    textIsItalic;

var loadedImage,
    scalingX,
    scalingY;

var currShape,
    shapeColor,
    shapePoint1X,
    shapePoint1Y,
    shapePoint2X,
    shapePoint2Y;

var stepStack = [],
    stepPointer = -1;


// Buttons hadle
function init_buttons() {
    var brush = document.getElementById("brush");
    brush.onmouseover = brush_hover;
    brush.onmouseout = brush_unhover;
    brush.onclick = brush_clicked;

    var eraser = document.getElementById("eraser");
    eraser.onmouseover = eraser_hover;
    eraser.onmouseout = eraser_unhover;
    eraser.onclick = eraser_clicked;

    var text = document.getElementById("text");
    text.onmouseover = text_hover;
    text.onmouseout = text_unhover;
    text.onclick = text_clicked;

    var shape = document.getElementById("shape");
    shape.onmouseover = shape_hover;
    shape.onmouseout = shape_unhover;
    shape.onclick = shape_clicked;

    var image = document.getElementById("image");
    image.onmouseover = image_hover;
    image.onmouseout = image_unhover;
    image.onclick = image_clicked;

    var prevStep = document.getElementById("prevstep");
    prevStep.onmousedown = function () { this.src = "design/previous_pressed.png" };
    prevStep.onmouseup = function () { this.src = "design/previous_default.png"; undo_canvas(); };
    prevStep.onmouseleave = function () { this.src = "design/previous_default.png" };

    var clear = document.getElementById("clear");
    clear.onmousedown = function () { this.src = "design/clear_pressed.png" };
    clear.onmouseup = function () { this.src = "design/clear_default.png"; clear_canvas(); };
    clear.onmouseleave = function () { this.src = "design/clear_default.png" };

    var save = document.getElementById("save");
    save.onmousedown = function () { this.src = "design/save_pressed.png" };
    save.onmouseup = function () { this.src = "design/save_default.png"; get_file_URL() };
    save.onmouseleave = function () { this.src = "design/save_default.png" };

    var nextStep = document.getElementById("nextstep");
    nextStep.onmousedown = function () { this.src = "design/next_pressed.png" };
    nextStep.onmouseup = function () { this.src = "design/next_default.png"; redo_canvas(); };
    nextStep.onmouseleave = function () { this.src = "design/next_default.png" };
}


function brush_hover() {
    if (buttonState[0] === false) {
        var brush = document.getElementById("brush");
        brush.setAttribute("src", "design/brush_hover.png")
    }
}
function brush_unhover() {
    if (buttonState[0] === false) {
        var brush = document.getElementById("brush");
        brush.setAttribute("src", "design/brush_default.png");
    }
}
function brush_clicked() {
    var brush = document.getElementById("brush");

    if (buttonState[0] === true) {
        buttonState[0] = false;
        brush.setAttribute("src", "design/brush_default.png");
        close_property("brush");
        switch_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++)     //disable another potentially active tool
        {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("eraser").setAttribute("src", "design/eraser_default.png");
        document.getElementById("text").setAttribute("src", "design/text_default.png");
        document.getElementById("shape").setAttribute("src", "design/shape_default.png");
        document.getElementById("image").setAttribute("src", "design/image_default.png");

        buttonState[0] = true;      //enable last clicked one
        brush.setAttribute("src", "design/brush_selected.png");

        switch_property("brush");      //switch property

        switch_cursor("brush");     //switch cursor
    }
}


function eraser_hover() {
    if (buttonState[1] === false) {
        var eraser = document.getElementById("eraser");
        eraser.setAttribute("src", "design/eraser_hover.png")
    }
}
function eraser_unhover() {
    if (buttonState[1] === false) {
        var eraser = document.getElementById("eraser");
        eraser.setAttribute("src", "design/eraser_default.png");
    }
}
function eraser_clicked() {
    var eraser = document.getElementById("eraser");

    if (buttonState[1] === true) {
        buttonState[1] = false;
        eraser.setAttribute("src", "design/eraser_default.png");
        close_property("eraser");
        switch_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++)     //disable another potentially active tool
        {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("brush").setAttribute("src", "design/brush_default.png");
        document.getElementById("text").setAttribute("src", "design/text_default.png");
        document.getElementById("shape").setAttribute("src", "design/shape_default.png");
        document.getElementById("image").setAttribute("src", "design/image_default.png");

        buttonState[1] = true;      //enable last clicked one
        eraser.setAttribute("src", "design/eraser_selected.png");

        switch_property("eraser");      //switch property

        switch_cursor("eraser");        //switch cursor
    }
}


function text_hover() {
    if (buttonState[2] === false) {
        var text = document.getElementById("text");
        text.setAttribute("src", "design/text_hover.png")
    }
}
function text_unhover() {
    if (buttonState[2] === false) {
        var text = document.getElementById("text");
        text.setAttribute("src", "design/text_default.png");
    }
}
function text_clicked() {
    var text = document.getElementById("text");

    if (buttonState[2] === true) {
        buttonState[2] = false;
        text.setAttribute("src", "design/text_default.png");
        close_property("text");
        switch_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++)     //disable another potentially active tool
        {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("brush").setAttribute("src", "design/brush_default.png");
        document.getElementById("eraser").setAttribute("src", "design/eraser_default.png");
        document.getElementById("shape").setAttribute("src", "design/shape_default.png");
        document.getElementById("image").setAttribute("src", "design/image_default.png");

        buttonState[2] = true;      //enable last clicked one
        text.setAttribute("src", "design/text_selected.png");

        switch_property("text");      //switch property

        switch_cursor("texttool");      //switch cursor
    }
}


function shape_hover() {
    if (buttonState[3] === false) {
        var shape = document.getElementById("shape");
        shape.setAttribute("src", "design/shape_hover.png")
    }
}
function shape_unhover() {
    if (buttonState[3] === false) {
        var shape = document.getElementById("shape");
        shape.setAttribute("src", "design/shape_default.png");
    }
}
function shape_clicked() {
    var shape = document.getElementById("shape");

    if (buttonState[3] === true) {
        buttonState[3] = false;
        shape.setAttribute("src", "design/shape_default.png");
        close_property("shape");
        switch_cursor("shape");
    }
    else {
        for (var i = 0; i < 5; i++)     //disable another potentially active tool
        {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("brush").setAttribute("src", "design/brush_default.png");
        document.getElementById("eraser").setAttribute("src", "design/eraser_default.png");
        document.getElementById("text").setAttribute("src", "design/text_default.png");
        document.getElementById("image").setAttribute("src", "design/image_default.png");

        buttonState[3] = true;      //enable last clicked one
        shape.setAttribute("src", "design/shape_selected.png");

        switch_property("shape");      //switch property

        switch_cursor("shape");      //switch cursor
    }
}


function image_hover() {
    if (buttonState[4] === false) {
        var image = document.getElementById("image");
        image.setAttribute("src", "design/image_hover.png")
    }
}
function image_unhover() {
    if (buttonState[4] === false) {
        var image = document.getElementById("image");
        image.setAttribute("src", "design/image_default.png");
    }
}
function image_clicked() {
    var image = document.getElementById("image");

    if (buttonState[4] === true) {
        buttonState[4] = false;
        image.setAttribute("src", "design/image_default.png");
        close_property("image");
        switch_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++)     //disable another potentially active tool
        {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("brush").setAttribute("src", "design/brush_default.png");
        document.getElementById("eraser").setAttribute("src", "design/eraser_default.png");
        document.getElementById("text").setAttribute("src", "design/text_default.png");
        document.getElementById("shape").setAttribute("src", "design/shape_default.png");

        buttonState[4] = true;      //enable last clicked one
        image.setAttribute("src", "design/image_selected.png");

        switch_property("image");      //switch property

        switch_cursor("imageloader");       //switch cursor
    }
}


// Property control
function init_properties() {
    //BRUSH
    brushSize = 10;     //initial value
    brushOpacity = 1;
    brushColor = 'rgb(0, 0, 0)';

    var bs = document.getElementById("brushsize");      //size & slider
    bs.innerHTML = brushSize;
    var sizeSlider = document.getElementById("brushsizeslider");
    sizeSlider.oninput = function () { brushSize = this.value; bs.innerHTML = brushSize; };

    var bo = document.getElementById("brushopacity");       //opacity & slider
    bo.innerHTML = brushOpacity;
    var opacitySlider = document.getElementById("brushopacityslider");
    opacitySlider.oninput = function () { brushOpacity = this.value; bo.innerHTML = brushOpacity; };

    var bcp = document.getElementById("brushcolorpicker");      //color picker
    bcp.setAttribute("color", brushColor);
    bcp.onchange = function () { brushColor = this.value; };


    //ERASER
    eraserSize = 15;
    eraserDepth = 1;

    var es = document.getElementById("erasersize");      //size & slider
    es.innerHTML = eraserSize;
    var eSizeSlider = document.getElementById("erasersizeslider");
    eSizeSlider.oninput = function () { eraserSize = this.value; es.innerHTML = eraserSize; };

    var ds = document.getElementById("eraserdepth");      //depth & slider
    ds.innerHTML = eraserDepth;
    var eDepthSlider = document.getElementById("eraserdepthslider");
    eDepthSlider.oninput = function () { eraserDepth = this.value; ds.innerHTML = eraserDepth; };

    
    //TEXT
    textFont = 'Georgia';
    textSize = 12;
    textColor = 'rgb(0, 0, 0)';
    textIsBold = false;
    textIsItalic = false;

    var tf = document.getElementById("textfont");       //font select
    tf.value = textFont;
    tf.onchange = function () { textFont = this.value; changeTextboxFont(textFont); };

    var ts = document.getElementById("textsize");       //text size  
    ts.value = textSize;
    ts.onchange = function () { textSize = this.value + 'px'; changeTextSize(textSize); };

    var tc = document.getElementById("textcolorpicker");      //text color
    tc.value = textColor;
    tc.onchange = function () { textColor = this.value; changeTextColor(textColor); };

    var tb = document.getElementById("textbold");       //bold & italic
    tb.onchange = function () { textIsBold = this.checked; changeTextStyle(textIsBold, textIsItalic) };
    var ti = document.getElementById("textitalic");
    ti.onchange = function () { textIsItalic = this.checked; changeTextStyle(textIsBold, textIsItalic) };

    //SHAPE
    currShape = 'Rectangle';
    shapeColor = 'rgb(0, 0, 0)';

    var ss = document.getElementById("shapeselector");
    ss.onchange = function () { currShape = this.value; };
    var scs = document.getElementById("shapecolorpicker");
    scs.onchange = function () { shapeColor = this.value; };

    //IMAGE LOADER
    loadedImage = null;
    scalingX = 1;
    scalingY = 1;
    shapePoint1X = 0, shapePoint1Y = 0, shapePoint2X = 0, shapePoint2Y = 0;

    var iss = document.getElementById("imagesourceselector");       //loader
    iss.onchange = function (e) { load_image(e); };
    var isx = document.getElementById("imagescalex");       //scaling x
    isx.value = scalingX;
    isx.onchange = function () { scalingX = this.value; };
    var isy = document.getElementById("imagescaley");       //scaling y
    isy.value = scalingY;
    isy.onchange = function () { scalingY = this.value; };


    close_property("any");      //hide properties by default
}

function switch_property(type) {
    switch (type) {
        case "brush":
            close_property("any");
            var brushProperty = document.getElementById("brushproperty");
            brushProperty.style.display = "inline";
            break;
        case "eraser":
            close_property("any");
            var eraserProperty = document.getElementById("eraserproperty");
            eraserProperty.style.display = "inline";
            break;
        case "text":
            close_property("any");
            var textProperty = document.getElementById("textproperty");
            textProperty.style.display = "inline";
            break;
        case "shape":
            close_property("any");
            var shapeProperty = document.getElementById("shapeproperty");
            shapeProperty.style.display = "inline";
            break;
        case "image":
            close_property("any");
            var imageProperty = document.getElementById("imageproperty");
            imageProperty.style.display = "inline";
            break;
    }
}

function close_property(type) {
    switch (type) {
        case "brush":
            var brushProperty = document.getElementById("brushproperty");
            brushProperty.style.display = "none";
            break;
        case "eraser":
            var eraserProperty = document.getElementById("eraserproperty");
            eraserProperty.style.display = "none";
            break;
        case "text":
            var textProperty = document.getElementById("textproperty");
            textProperty.style.display = "none";
            break;
        case "shape":
            var shapeProperty = document.getElementById("shapeproperty");
            shapeProperty.style.display = "none";
            break;
        case "image":
            var imageProperty = document.getElementById("imageproperty");
            imageProperty.style.display = "none";
            break;
        case "any":
            var properties = document.getElementsByClassName("property");
            for (i = 0; i < properties.length; i++) {
                properties[i].style.display = "none";
            }
    }
}


// Canvas tools
function init_tools() {
    var canvas = document.getElementById("canvas");

    if (!canvas.getContext) {
        console.log("Your browser don't support canvas");   //check compatibility
        document.onfullscreenerror();
    }
    else {
        //add listener to record mouse event
        canvas.addEventListener("mousemove", function (e) { mouse_moved(e) }, false);
        canvas.addEventListener("mousedown", function (e) { mouse_down(e) }, false);
        canvas.addEventListener("mouseup", function (e) { mouse_up(e) }, false);
        canvas.onmouseleave = function () { mouseDown = false; /*push_step_stack()*/ };   //moveout while pressed
    }
}

function mouse_moved(e) {
    var canvas = document.getElementById("canvas");
    pX = cX;
    pY = cY;
    cX = e.clientX - canvas.offsetLeft;
    cY = e.clientY - canvas.offsetTop;

    //BRUSH
    if (buttonState[0]) {
        if (mouseDown) {
            //record pos and draw line
            var ctx = document.getElementById("canvas").getContext('2d');
            ctx.beginPath();
            ctx.moveTo(pX, pY);
            ctx.lineTo(cX, cY);
            ctx.stroke();
        }
    }
    //ERASER
    else if (buttonState[1]) {
        if (mouseDown) {
            //record pos and draw line
            var ctx = document.getElementById("canvas").getContext('2d');
            ctx.beginPath();
            ctx.moveTo(pX, pY);
            ctx.lineTo(cX, cY);
            ctx.stroke();
        }
    }

}

function mouse_down(e) {
    var canvas = document.getElementById("canvas");
    mouseDown = true;
    cX = e.clientX - canvas.offsetLeft;
    cY = e.clientY - canvas.offsetTop;


    //BRUSH
    if (buttonState[0]) {
        //new path with brush color
        var ctx = document.getElementById("canvas").getContext('2d');
        ctx.lineWidth = brushSize;
        ctx.lineCap = 'round';
        ctx.strokeStyle = brushColor;
        ctx.globalAlpha = brushOpacity;

        ctx.globalCompositeOperation = 'source-over';
        ctx.beginPath();
        ctx.moveTo(cX, cY);
    }
    //ERASER
    else if (buttonState[1]) {
        //new path with transparent color using copy type
        var ctx = document.getElementById("canvas").getContext('2d');
        ctx.lineWidth = eraserSize;
        ctx.lineCap = 'round';
        ctx.strokeStyle = 'rgba(255, 255, 255, 255)';
        ctx.globalAlpha = eraserDepth;

        ctx.globalCompositeOperation = 'destination-out';
        ctx.beginPath();
        ctx.moveTo(cX, cY);
    }
    //TEXT
    else if (buttonState[2]) {
        var Xrec = cX, Yrec = cY;       //store first clicked pos

        if (!entering) {
            entering = true;
            mouseDown = false;      //mimic mouseup event (cuz now focusing on input)
            //create text input box
            var input = document.createElement('input');
            input.type = 'text';
            input.id = "textbox";
            input.style.position = 'absolute';
            input.style.left = cX + canvas.offsetLeft + 'px';
            input.style.top = cY + canvas.offsetTop + 'px';
            input.style.fontFamily = textFont;
            input.style.fontSize = textSize;
            input.style.color = textColor;
            input.style.fontWeight = (textIsBold === true ? 'bold' : 'normal');
            input.style.fontStyle = (textIsItalic === true ? 'italic' : 'normal');
            input.onkeydown = function (e) {
                if (e.key === 'Enter') {
                    entering = false;
                    drawTextOnCanvas(this.value, Xrec, Yrec, this.style.fontFamily, this.style.fontSize,
                        this.style.color, this.style.fontWeight, this.style.fontStyle);
                } else return;
            };

            document.body.appendChild(input);
            input.focus();
        }
        else {
            entering = false;
            //give up drawing text
            document.body.removeChild(document.getElementById("textbox"));
        }
    }
    //SHAPE
    else if (buttonState[3]) {
        shapePoint1X = cX,      //record mousedown point
        shapePoint1Y = cY;
    }
    //IMAGE LOADER
    else if (buttonState[4]) {
        //window.alert(loadedImage.src);
        if (loadedImage.src !== '') {
            var ctx = document.getElementById("canvas").getContext('2d');
            ctx.globalCompositeOperation = 'source-over';
            ctx.globalAlpha = 1;
            var img = new Image();
            img.src = "design/icon_si.png";
            ctx.drawImage(loadedImage, cX, cY,
                loadedImage.width * scalingX, loadedImage.height * scalingY);
            push_step_stack();
        }
    }
}

function mouse_up(e) {
    var canvas = document.getElementById("canvas");
    mouseDown = false;
    cX = e.clientX - canvas.offsetLeft;
    cY = e.clientY - canvas.offsetTop;

    if (buttonState[3]) {
        shapePoint2X = cX;      //record mouseup point
        shapePoint2Y = cY;

        var ctx = document.getElementById("canvas").getContext('2d');
        ctx.globalCompositeOperation = "source-over";
        ctx.globalAlpha = 1;
        ctx.fillStyle = shapeColor;
        switch (currShape) {        //draw shape
            case 'Rectangle':
                ctx.beginPath();
                ctx.moveTo(shapePoint1X, shapePoint1Y);
                ctx.lineTo(shapePoint2X, shapePoint1Y);
                ctx.lineTo(shapePoint2X, shapePoint2Y);
                ctx.lineTo(shapePoint1X, shapePoint2Y);
                ctx.fill();
                break;
            case 'Circle':
                var dx = Math.abs(shapePoint1X - shapePoint2X),
                    dy = Math.abs(shapePoint1Y - shapePoint2Y);
                var midPointX = shapePoint1X + dx / 2,
                    midPointY = shapePoint1Y + dy / 2;
                var radius = Math.sqrt(dx * dx + dy * dy) / 2;
                ctx.beginPath();
                ctx.arc(midPointX, midPointY, radius, 0, Math.PI * 2, true);
                ctx.fill();
                break;
            case 'Triangle':
                ctx.beginPath();
                ctx.moveTo(shapePoint1X, shapePoint1Y);
                ctx.lineTo(shapePoint2X, shapePoint2Y);
                var dx = Math.abs(shapePoint1X - shapePoint2X);
                ctx.lineTo(shapePoint2X - 2 * dx, shapePoint2Y);
                ctx.fill();
                break;
        }
    }

    if (buttonState[0] || buttonState[1] || buttonState[3]) {     //keep track of steps
        push_step_stack();
    }
}


//help functions
function drawTextOnCanvas(text, x, y, font, size, color, weight, style) {
    var textToDraw = text;
    document.body.removeChild(document.getElementById("textbox"));
    

    var ctx = document.getElementById("canvas").getContext('2d');
    ctx.font = weight + " " + style + " " + size + " " + font;
    ctx.fillStyle = color;
    ctx.textBaseline = 'top';   //offset: original of canvas text is at bottom left
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = 1;
    ctx.fillText(textToDraw, x, y); 

    push_step_stack();
}

function changeTextboxFont(font) {
    if (document.getElementById("textbox")) {
        var textBox = document.getElementById("textbox");
        textBox.style.fontFamily = font;
    }
}

function changeTextSize(size) {
    if (document.getElementById("textbox")) {
        var textBox = document.getElementById("textbox");
        textBox.style.fontSize = size;
    }
}

function changeTextColor(color) {
    if (document.getElementById("textbox")) {
        var textBox = document.getElementById("textbox");
        textBox.style.color = color;
    }
}

function changeTextStyle(isBold, isItalic) {
    if (document.getElementById("textbox")) {
        var textBox = document.getElementById("textbox");
        textBox.style.fontWeight = (isBold === true ? 'bold' : 'normal');
        textBox.style.fontStyle = (isItalic === true ? 'italic' : 'normal');
    }
}

function load_image(event) {
    var URL = window.webkitURL || window.URL;       //get around with security problem
    var url = URL.createObjectURL(event.target.files[0]);

    var img = new Image();
    img.src = url;
    loadedImage = img;

    scalingX = 1, scalingY = 1;     //reset ratio everytime
    var isx = document.getElementById("imagescalex");
    isx.value = scalingX;
    var isy = document.getElementById("imagescaley");
    isy.value = scalingY;
}

function clear_canvas() {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);
}

function get_file_URL() {
    var link = document.getElementById("link");
    link.href = document.getElementById("canvas").toDataURL();
    link.download = 'pic.png';
}

function push_step_stack() {
    var cavasImgURL = document.getElementById("canvas").toDataURL();
    var canvasImage = new Image();
    canvasImage.src = cavasImgURL;
    stepPointer++;

    if (stepPointer > (stepStack.length - 1)) {
        stepStack.push(canvasImage);
    }
    else {
        stepStack[stepPointer] = canvasImage;
    }
    
}

function undo_canvas() {
    if (stepPointer !== 0) {
        clear_canvas();
        var ctx = document.getElementById("canvas").getContext('2d');
        ctx.drawImage(stepStack[--stepPointer], 0, 0);
    }
    else {
        window.alert("no previous step!");
    }
}

function redo_canvas() {
    if ((stepPointer + 1) <= (stepStack.length - 1)) {
        clear_canvas();
        var ctx = document.getElementById("canvas").getContext('2d');
        ctx.drawImage(stepStack[++stepPointer], 0, 0);
    }
    else {
        window.alert("no next step!");
    }
}

function switch_cursor(type) {
    var canvas = document.getElementById("canvas");

    switch (type) {
        case "brush":
            canvas.style.cursor = "url(design/cursor_brush.png), auto";
            break;
        case "eraser":
            canvas.style.cursor = "url(design/cursor_eraser.png), auto";
            break;
        case "texttool":
            canvas.style.cursor = "url(design/cursor_texttool.png), auto";
            break;
        case "imageloader":
            canvas.style.cursor = "url(design/cursor_imageloader.png), auto";
            break;
        case "shape":
            canvas.style.cursor = "url(design/cursor_shapetool.png), auto";
            break;
        case "none":
            canvas.style.cursor = "default";
            break;
    }
}